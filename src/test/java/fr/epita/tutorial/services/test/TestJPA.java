package fr.epita.tutorial.services.test;

import java.util.List;

import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.epita.tutorial.datamodel.Author;
import fr.epita.tutorial.services.data.AuthorDAO;
import fr.epita.tutorial.services.data.BookDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext.xml")
public class TestJPA {
	
	private static final Logger LOGGER = LogManager.getLogger(TestJPA.class);

	@Inject
	AuthorDAO authorDAO;
	
	@Inject 
	BookDAO bookDAO;
	
	
	@Before
	public void addAuthor() {
    	Author author = new Author();
    	author.setFirstName("Philip");
    	author.setLastName("K. Dick");
    	authorDAO.create(author);
	}
	
    @Test
    public void testAuthorDAOCreateGetById() {
    	
    	// Given
    	Author author = new Author();
    	author.setFirstName("Stephen");
    	author.setLastName("King");
    	
    	// When
        Long id = authorDAO.create(author);
        
        // then
        author = authorDAO.getById(id);
        LOGGER.info(author);
        Assert.assertEquals("Stephen", author.getFirstName());
        Assert.assertEquals("King", author.getLastName());
    }
    
    @Test
    public void testAuthorDAOSearchUpdate() {
    	
    	// Given
    	Author author = new Author();
    	author.setFirstName("Philip");
        author = authorDAO.search(author).get(0);
        
        // When
        author.setFirstName("Howard");
        author.setLastName("Lovecraft");
        authorDAO.update(author);

        // Then 
        author = authorDAO.search(author).get(0);
        LOGGER.info(author);
        Assert.assertEquals("Howard", author.getFirstName());
        Assert.assertEquals("Lovecraft", author.getLastName());
    }
    
    @Test
    public void testAuthorDAODeleteGetAll() {
    	// Given
    	List<Author> authors = authorDAO.getAll();
        
        // When
    	for (Author author : authors)
    		authorDAO.delete(author);

        // Then 
        authors = authorDAO.getAll();
        LOGGER.info(authors);
        Assert.assertEquals(0, authors.size());
    }
}
