package fr.epita.tutorial.services.data;

import java.util.List;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import fr.epita.tutorial.datamodel.Author;

@Repository
public class AuthorDAO extends DAO<Author> {

	public AuthorDAO() {
		this.setmodelClass(Author.class);
	}

	@Override
	public List<Author> search(Author criteria) {
		String searchQuery = "from Author as a where a.firstName like :firstName or a.lastName like :lastName";
		Session session = this.getSession();
		Query<Author> query = session.createQuery(searchQuery, Author.class);
		query.setParameter("firstName", "%" + criteria.getFirstName() + "%");
		query.setParameter("lastName", "%" + criteria.getLastName() + "%");
		return query.list();
	}
}
