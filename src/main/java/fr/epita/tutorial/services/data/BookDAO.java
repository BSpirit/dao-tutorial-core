package fr.epita.tutorial.services.data;

import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import fr.epita.tutorial.datamodel.Book;

@Repository
public class BookDAO extends DAO<Book> {
	
	public BookDAO() {
		this.setmodelClass(Book.class);
	}

	@Override
	public List<Book> search(Book criteria) {
		String searchQuery = "from Book where author = :author";
		Query<Book> query = getSession().createQuery(searchQuery, Book.class);
		query.setParameter("question", criteria.getAuthor());
		return query.list();
	}
}
