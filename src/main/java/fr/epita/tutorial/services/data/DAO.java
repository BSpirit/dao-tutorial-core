package fr.epita.tutorial.services.data;

import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;

public abstract class DAO<T> {

	protected static final Logger LOGGER = LogManager.getLogger(DAO.class);
	protected Class<T> modelClass;

	@Inject
	@Named("sessionFactory")
	private SessionFactory sf;

	public void setmodelClass(Class<T> modelClass) {
		this.modelClass = modelClass;
	}

	protected final Session getSession() {
		Session session = null;
		try {
			session = this.sf.getCurrentSession();
		} catch (Exception e) {
			LOGGER.error(e);
		}

		if (session == null)
			session = sf.openSession();

		return session;

	}

	protected final Transaction getTransaction(Session session) {
		Transaction tx = session.getTransaction();
		if (!TransactionStatus.ACTIVE.equals(tx.getStatus()))
			tx = session.beginTransaction();

		return tx;
	}

	public final Long create(T obj) {
		Session session = this.getSession();
		Transaction tx = this.getTransaction(session);
		Long id = (Long) session.save(obj);
		tx.commit();
		return id;
	}

	public final void delete(T obj) {
		Session session = this.getSession();
		Transaction tx = this.getTransaction(session);
		session.delete(obj);
		tx.commit();
	}

	public final void update(T obj) {
		Session session = this.getSession();
		Transaction tx = this.getTransaction(session);
		session.update(obj);
		tx.commit();
	}

	public T getById(Long id) {
		return getSession().get(modelClass, id);
	}

	@SuppressWarnings("unchecked")
	public List<T> getAll() {
		return getSession().createQuery("from " + modelClass.getName()).list();
	}
	
	public abstract List<T> search(T criteria);
}
